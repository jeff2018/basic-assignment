import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.css']
})
export class BasicComponent implements OnInit {

  userCreate = 'No username was created!';
  username = '';

  constructor() {
  }

  ngOnInit() {
  }

  onCreateUser() {
    this.userCreate = 'Username was created! Name is ' + this.username;
    return this.username = '';
  }

}
